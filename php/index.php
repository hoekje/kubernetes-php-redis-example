<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require 'Predis/Autoloader.php';
Predis\Autoloader::register();

$host = 'redis-slave';
$client = new Predis\Client([
  'scheme' => 'tcp',
  'host'   => $host,
  'port'   => 6379,
]);

echo json_encode(['last_access' => $client->get('last_access')]);


// Write to master
$host = 'redis-master';
$client = new Predis\Client([
  'scheme' => 'tcp',
  'host'   => $host,
  'port'   => 6379,
]);

$client->set('last_access', time());
