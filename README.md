https://kubernetes.io/docs/tutorials/stateless-application/guestbook/

# Example kubernetes hosting

With php-5/apache as frontend, redis for caching

# Create namespaces for prod, acc, dev

kubectl create -f prod.namespace.yml
kubectl create -f acc.namespace.yml
kubectl create -f dev.namespace.yml

# Create services and deployments in prod, acc, dev

kubectl --namespace=prod create -f .
kubectl --namespace=acc create -f .
kubectl --namespace=dev create -f .

# Look!

minikube --namespace=prod service frontend --url
